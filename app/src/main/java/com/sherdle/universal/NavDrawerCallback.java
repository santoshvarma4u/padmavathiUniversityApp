package com.sherdle.universal;

public interface NavDrawerCallback {
    void onNavigationDrawerItemSelected(int position, NavItem item);
}
